persp-projectile (1:1.0.0+git20210618.4e374d7-3) UNRELEASED; urgency=medium

  * Adopt package and add myself as Uploader replacing Sean (Closes: #904233)
  * Drop emacs version from recommends which was since oldoldstable
  * Drop emacs from depends following existing practices

 -- Xiyue Deng <manphiz@gmail.com>  Wed, 13 Mar 2024 01:07:22 -0700

persp-projectile (1:1.0.0+git20210618.4e374d7-2) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.5.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 25 Jul 2024 23:21:55 +0900

persp-projectile (1:1.0.0+git20210618.4e374d7-1) unstable; urgency=medium

  * Team upload.

  [ Nicholas D Steeves ]
  * Drop emacs24 from Enhances (package does not exist in bullseye).

  [ Xiyue Deng ]
  * Team upload.
  * Sync to latest upstream head.
    - Fix compatibility with elpa-perspective.  Closes: #919035.
    - Refresh patches.
  * Modernize d/watch.
  * Migrate from d/compat to debhelper-compat and update to version 13.
  * Fix typo in d/control.
  * Update Standards-Version to 4.6.2.  No change needed.
  * Drop parameter "--parallel" in d/rules.
  * Drop Built-Using from binary package.
    This package has no relevant licensing requirements.
  * Drop unused and update renamed lintian overrides.
  * Update year and Upstream-Contact in d/copyright.
  * Add d/upstream/metadata.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sat, 24 Feb 2024 11:51:42 +0800

persp-projectile (1:0.2.0-4) unstable; urgency=medium

  * Team upload

  [ David Krauser ]
  * Update maintainer email address

  [ Dhavan Vaidya ]
  * d/control: Change Vcs-{Browser,Git} URL to salsa.debian.org

  [ David Bremner ]
  * Rebuild with dh-elpa 2.x

 -- David Bremner <bremner@debian.org>  Sun, 24 Jan 2021 07:20:16 -0400

persp-projectile (1:0.2.0-3) unstable; urgency=medium

  * Team upload.
  * Rebuild with current dh-elpa

 -- David Bremner <bremner@debian.org>  Mon, 26 Aug 2019 12:57:58 -0300

persp-projectile (1:0.2.0-2) unstable; urgency=medium

  * Team upload.
  * Rebuild with dh-elpa 1.13 to fix byte-compilation with unversioned
    emacs

 -- David Bremner <bremner@debian.org>  Sat, 02 Jun 2018 21:18:09 -0300

persp-projectile (1:0.2.0-1) unstable; urgency=medium

  * Initial release (new source package split from src:projectile).
  * Add epoch because previous binary package version was 0.13.0-1.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sun, 24 Jul 2016 19:46:32 -0700
